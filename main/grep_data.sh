#!/bin/bash
# 
# Usage:
#
# ./grep_data.sh output_file_simulation
#

file=$1

grep "dH"   $file | awk '{print $4}'  > dH.dat
grep "plaq" $file | awk '{print $8}'  > plaq.dat
grep "qtop" $file | awk '{print $10}' > qtop.dat
grep "Force FRF_TOT:" $file  | awk -F'[, \t]*' '{print $5}' > FRF_TOT.dat
grep "Force FRG:"     $file  | awk -F'[, \t]*' '{print $5}' > FRG.dat

