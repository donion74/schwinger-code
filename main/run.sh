#!/bin/bash

#SBATCH --job-name=test
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --gres=gpu:1
#SBATCH --output=job.%j.out # Stdout (%j=jobId)
#SBATCH --error=job.%j.err # Stderr (%j=jobId
#SBATCH -A lap21
#SBATCH --reservation=lap21

module load CUDA/11.1.1-GCC-10.2.0
srun cuda-gdb –version

./schwinger infile > output.dat &

wait

exit 0

